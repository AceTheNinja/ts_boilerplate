var gulp = require('gulp');
var yaml = require('gulp-yaml');

gulp.task('default', function() {
    gulp.src('./api/swagger/swagger.yaml')
        .pipe(yaml({ safe: true }))
        .pipe(gulp.dest('./public/'))
        .pipe(gulp.dest('./dist/public'));
});