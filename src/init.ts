const http = require("http")
const appServer = require("./server")

const port = process.env.PORT

appServer.set("port", port)

const server = http.createServer(appServer)

server.listen(port, "0.0.0.0")
server.on("error", onError)
server.on("listening", onListening)

function onError(error: any) {
    if (error.syscall !== "listen") throw error
    const bind = (typeof port === "string") ? "Pipe " + port : "Port " + port
    switch (error.code) {
    case "EACCES":
        console.error(`${bind} requires elevated privileges`)
        process.exit(1)
    case "EADDRINUSE":
        console.error(`${bind} is already in use`)
        process.exit(1)
    default:
        throw error
    }
}

function onListening() {
    console.log(`Server started listening on port ${port}`)
}
