import * as dotenv from "dotenv"

const dev: any = {
    url: "http://localhost:3003",
    database: "mongodb://localhost:27017/dev",
    mongooseDebug: false,
    PORT: 3003,
};

export default {
    load: () => {
        if (process.env.NODE_ENV !== "prod") {
            Object.keys(dev).forEach(function (key: string) {
                if (!process.config.hasOwnProperty(key)) {
                    process.env[key] = dev[key]
                } else {
                    console.log(key, " already exists");
                    process.exit(1);
                }
            })
        } else {
            dotenv.config();
        }
    }
};