import chalk from "chalk"

const logMsg = chalk.bold.white
const errorMsg = chalk.white.bgRed

const env = process.env.NODE_ENV

function debugLog(msg: any) {
    console.log(logMsg(`${new Date()} | LOG: `, msg))
}

function debugError(msg: string) {
    console.error(errorMsg(`${new Date()} | ERROR:`, msg))
}

// TODO: Logging for prod

function prodLog(msg: any) {

}

function prodError(msg: any) {

}

function log(msg: any) {
    if (env !== "prod") {
        debugLog(msg)
    } else {
        prodLog(msg)
    }
}

function error(msg: any) {
    if (env !== "prod") {
        debugError(msg)
    } else {
        prodError(msg)
    }
}

export {
    log,
    error
};
