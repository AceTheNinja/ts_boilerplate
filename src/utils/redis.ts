import * as Redis from "redis";
const redisConfig = process.env.redisConfig;

const redis = Redis.createClient(redisConfig);

export default {
    get(key: string) {
        return new Promise((resolve, reject) => {
            redis.get(key, (error: any, data: any) => {
                if (error) {
                    console.error(`error while reading from redis ${JSON.stringify(error)}`);
                    return resolve();
                }
                try {
                    return resolve(data && JSON.parse(data));
                } catch (e) {
                    return reject(e);
                }
            });
        });
    },
    set(key: string, data: any, ttl: number) {
        return new Promise((resolve, reject) => {
            try {
                if (ttl) {
                    redis.set(key, JSON.stringify(data), "EX", ttl);
                } else {
                    redis.set(key, JSON.stringify(data));
                }
                return resolve();
            } catch (e) {
                return reject(e);
            }
        });
    },
    lpush(key: string, value: any) {
        return new Promise((resolve, reject) => {
            try {
                redis.lpush(key, value);
                if (Math.random() > 0.2) {
                    redis.ltrim(key, 0, 4);
                }

                return resolve();
            } catch (ex) {
                return reject();
            }
        })
    },
    lrange(key: string) {
        return new Promise((resolve, reject) => {
            redis.lrange(key, 0, 4, (error: any, data: any) => {
                if (error) {
                    return reject("Error fetching from redis")
                }

                return resolve(data);
            })
        })
    }
}