import * as jwt from "jsonwebtoken"
import IRequest from "../interfaces/modules/Request";
import { Response, NextFunction } from "express";

const verifyToken = async (token: string, secret: string): Promise<any> => {
    let decodedToken = undefined;
    await jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        console.log(err);
      } else {
        decodedToken = decoded;
      }
    });

    return decodedToken;
}

export const embedToken = (secret: string) => {
    return async function (req: IRequest, res: Response, next: NextFunction) {

      if (req.headers.authorization) {
        const authToken = req.headers.authorization;

        if (!authToken) {
          res.status(405).json({
            code: 405,
            msg: "Token Malformed!"
          })
        }

        const tokenData = await verifyToken(authToken, secret);

        // invalid token
        if (typeof tokenData === "undefined") {
          return res.status(405).json({
            code: 405,
            msg: "Please Signin again"
          });
        } else {
          if (tokenData.type === "refresh") {
            req.refreshToken = tokenData;
          } else {
            req.accessToken = tokenData;
          }
          next();
        }
      } else {
        next();
      }

    };
}
