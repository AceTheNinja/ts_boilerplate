import { Response, NextFunction } from "express";
import IRequest from "../interfaces/modules/Request";

class AccessController {

    private checkAuthorization(access_level: string, accessibleResources: any, resource: string) {
        accessibleResources = accessibleResources.split(" ");
        if (
            !["super-admin", "admin", "customer-care"].includes(access_level) ||
            !accessibleResources.includes(resource) &&
            accessibleResources[0] !== "*"
        ) return false;
        else return true;
    }

    public checkAuthorizationForResource(resource: string): any {
        return (req: IRequest, res: Response, next: NextFunction) => {
            let { accessibleResources } = req.accessToken;
            accessibleResources = accessibleResources.split(" ");
            if (accessibleResources.includes(resource) || accessibleResources[0] === "*")
                next();
            else
                return res.status(401).json({
                    code: 401, err: "Unauthorized"
                });
        }
    }

    public compareIdAndParamsId(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken &&
            (req.accessToken._id === req.params.id || req.accessToken.access_level === "super-admin")
        ) {
            next();
        } else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }
    public compareIdAndBodyId(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken &&
            (req.accessToken._id === req.body.userId || req.accessToken.access_level === "super-admin")
        ) {
            next();
        } else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }

    public compareIdAndQueryId(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken &&
            (req.accessToken._id === req.query.userId || req.accessToken.access_level === "super-admin")
        ) {
            next();
        } else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }

    public admin(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken && req.accessToken.access_level === "admin") next();
        else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }
    public superAdmin(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken && req.accessToken.access_level === "super-admin") next();
        else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }

    public customerCare(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken && req.accessToken.access_level === "customer-care") next();
        else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }
    public client(req: IRequest, res: Response, next: NextFunction) {
        if (req.accessToken && req.accessToken.access_level === "client") next();
        else {
            return res.status(401).json({
                code: 401,
                err: "Unauthorized"
            });
        }
    }

}

const accessController = new AccessController();
export default accessController;