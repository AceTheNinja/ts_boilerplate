import * as random from "random"

import UserService from "./service"
import OtpService from "./otp/service"

import IRequest from "../../interfaces/modules/Request"
import { Response } from "express"

export default class UserController {

    otpService: OtpService;
    userService: UserService

    constructor() {
        this.userService = new UserService()
        this.otpService = new OtpService()
    }

    public get = async (req: IRequest, res: Response) => {
        const { filter } = req.query;
        const users = await this.userService.find(filter);

        res.json(users);
    }

    public createUser = async (req: IRequest, res: Response) => {
        try {
            const user = req.body;
            const newUser = await this.userService.createUser(user);

            res.json(newUser);
        } catch (ex) {
            console.log(ex);
            res.sendStatus(400);
        }
    }


    public login = async (req: IRequest, res: Response) => {}
}