import { Router } from "express";
import UserController from "./controller";
import AccessController from "../access.controller";
class UserRoutes {

    router: Router;
    userController: UserController;

    constructor() {
        this.userController = new UserController();
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.post("/", this.userController.createUser);
        this.router.get("/", AccessController.superAdmin, this.userController.get);
        this.router.post("/login", this.userController.login);
    }
}

const userRoutes = new UserRoutes();
userRoutes.routes();

export default userRoutes.router;