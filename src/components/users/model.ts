import { Document, Schema, Model, model } from "mongoose";
import * as bcrypt from "bcrypt-nodejs";
import { IUser } from "../../interfaces/models/User";

export interface IUserModel extends IUser, Document {
    [key: string]: any;
    comparePassword?(candidatePassword: string, cb: any): any;
}

export const UserSchema: Schema = new Schema({
    email: { type: String },
    password: { type: String },
    contact: { type: String, index: true, unique: true },
    name: { type: String },
    dob: String,
    gender: String,
    address: String,
    imgUrl: { type: String, default: "http://i68.tinypic.com/5x4sxs.png" },
    inviteCode: { type: String }
}, { timestamps: true });

/**
 * Password hash middleware.
 */
UserSchema.pre("save", function save(next) {
    const user: IUserModel = this;
    if (!user.isModified("password")) { return next(); }
    bcrypt.genSalt(12, (err, salt) => {
        if (err) { return next(err); }
        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if (err) { return next(err); }
            user.password = hash;
            next();
        });
    });
});

/**
 * Helper method for validating user"s password.
 */
UserSchema.methods.comparePassword = function comparePassword(candidatePassword: string, cb: any) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        cb(err, isMatch);
    });
};

export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);