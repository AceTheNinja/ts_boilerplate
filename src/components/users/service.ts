import { User, IUserModel } from "./model"

export default class Service {

    public async find(filter: any) {
        return User.find({ ...filter });
    }
    public findByContact(contact: string) {
        return User.findOne({ contact })
    }

    public createUser(user: any) {
        return User.create(user);
    }

}