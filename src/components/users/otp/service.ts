import { Otp, IOtpModel } from "./model"

export default class OtpService {
    public findByContact(contact: string) {
        return Otp.findOne({ contact })
    }

    public async matchOtp(contact: string, otp: string) {
        const model = await Otp.findOne({ contact, otp, expiry: { $gt: Date.now() }, usedOn: null });

        if (!model) {
            return false;
        }

        return true;
    }

    public updateOtp(contact: string, otp: string) {
        return Otp.findOneAndUpdate({ contact }, { expiry: Date.now() + 54000, otp, usedOn: null }, { upsert: true, new: true });
    }

    public update(filter: any, modifiedFields: any) {
        return Otp.findOneAndUpdate(filter, { $set: { ...modifiedFields } }, { new: true });
    }
}