import { Document, Schema, Model, model } from "mongoose";

export interface IOtpModel extends Document {
    [key: string]: any;
}

export const OtpSchema: Schema = new Schema({
    contact: { type: String, unique: true },
    otp: { type: String, unique: true },
    expiry: { type: Date, default: Date.now() + 54000 },
    usedOn: { type: Date }
}, { timestamps: true });
export const Otp: Model<IOtpModel> = model<IOtpModel>("Otp", OtpSchema);