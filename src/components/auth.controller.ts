import * as jwt from "jsonwebtoken";

class AuthController {
    public async generateToken(data: any) {
        const accessToken = jwt.sign(this.setUserInfo(data._id, data.access_level, data.accessibleResources, "auth"), process.env.secret, {
            expiresIn: 86400 * 7
        })

        const refToken = jwt.sign(this.setUserInfo(data._id, data.access_level, data.accessibleResources, "refresh"), process.env.secret, {
            expiresIn: 86400 * 30
        })

        return { accessToken, refToken }
    }

    private setUserInfo(userId: any, access_level: string, accessibleResources: string, token_type: string): any {
        return {
            _id: userId.toString(),
            access_level: access_level || "user",
            accessibleResources: accessibleResources || null,
            token_type: token_type
        }
    }
}

const authController = new AuthController();

export default authController;