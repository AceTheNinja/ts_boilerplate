import * as EventEmitter from "events"

class EventsController {
    eventEmitter: EventEmitter = new EventEmitter();

    constructor() {
        this.eventEmitter.on("eventName", this.eventFunction);
    }

    eventFunction() {

    }
}

const eventsController = new EventsController();

export default eventsController.eventEmitter;