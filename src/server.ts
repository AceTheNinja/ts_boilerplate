import IRequest from "./interfaces/modules/Request";
import { NextFunction, Response } from "express";

/**
 * Module dependencies.
 */
import * as express from "express"
import * as compression from "compression"
import * as bodyParser from "body-parser"
import * as helmet from "helmet"
import * as cors from "cors"
import * as morgan from "morgan"
import * as mongoose from "mongoose"

import * as expressValidator from "express-validator"
import * as expressSanitized from "express-sanitized"

/**
 * Load Config and Services
 */
import config from "./config/config"
import * as logger from "./utils/logger"

config.load();
/**
 * Import Utils
 */
import { embedToken } from "./utils/auth"

/**
 * Import Routes
 */
import UserRoutes from "./components/users/router"


class Server {
    app: any
    constructor() {
        this.app = express()
        this.middleware()
        this.routes()
    }

    middleware() {
        mongoose.connect(process.env.database, {
            useNewUrlParser: true
        })
        mongoose.connection.on("error", () => {
            console.log("MongoDB connection error. Please make sure MongoDB is running.")
            process.exit(1)
        })
        mongoose.set("debug", process.env.mongooseDebug)

        this.app.set("port", process.env.PORT || 3000)
        this.app.use(compression())
        this.app.use(morgan("combined"))
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({
            extended: true
        }))
        this.app.use(expressSanitized())
        this.app.use(expressValidator())
        this.app.use(helmet())
        this.app.use(cors())

        this.app.use((req: IRequest, res: Response, next: NextFunction) => {
            console.log("Token: ", req.headers.authorization);
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials")
            res.header("Access-Control-Allow-Credentials", "true")
            next()
        })
        this.app.use(embedToken(process.env.secret))
    }

    routes() {
        this.app.use("/ping", (req: IRequest, res: Response) => {
            res.send("pong")
        })

        this.app.use("/users", UserRoutes)

        this.app.use((err: any, req: IRequest, res: Response, next: NextFunction) => {
            logger.error(JSON.stringify(err))
            if (process.env.NODE_ENV !== "prod") {
                res.status(500).send(err.fullError)
            } else {
                res.status(500).send(err.msg)
            }
        })
    }
}

module.exports = new Server().app
