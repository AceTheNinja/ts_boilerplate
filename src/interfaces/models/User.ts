export interface IUser {
    email?: string;
    password?: string;
    name?: string;
    contact?: string;
    imgUrl?: string;
    credits?: number;
    login_attempts?: number;
    access_level?: string;
}