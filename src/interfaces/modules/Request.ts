import { Request } from "express";

export default interface IRequest extends Request {
    accessToken?: any;
    refreshToken?: any;
    file: any;
}